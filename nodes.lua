-- Apple wood items
minetest.register_node("thin_decor:thin_wood_wall", {
	description = "Thin Wooden Wall",
	inventory_image = "default_wood.png",
	wield_image = "default_wood.png",

	tiles = { "default_wood.png" },
	drawtype = "nodebox",
	paramtype2 = "facedir",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, -0.5,	0.5, 0.5, -0.4}
		}
	},
	is_ground_content = true,
	groups = {choppy=1},
	drop = "thin_decor:thin_wood_wall"
})

minetest.register_node("thin_decor:thin_wood_slab_bottom", {
	description = "Thin Wooden Slab Bottom",
	inventory_image = "default_wood.png",
	wield_image = "default_wood.png",

	tiles = { "default_wood.png" },
	drawtype = "nodebox",
	paramtype2 = "facedir",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, -0.5,	0.5, -0.4, 0.5}
		}
	},
	is_ground_content = true,
	groups = {choppy=1},
	drop = "thin_decor:thin_wood_slab_bottom"
})

minetest.register_node("thin_decor:thin_wood_slab_top", {
	description = "Thin Wooden Slab Top",
	inventory_image = "default_wood.png",
	wield_image = "default_wood.png",

	tiles = { "default_wood.png" },
	drawtype = "nodebox",
	paramtype2 = "facedir",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.5, 0.4, -0.5,	0.5, 0.5, 0.5}
		}
	},
	is_ground_content = true,
	groups = {choppy=1},
	drop = "thin_decor:thin_wood_slab_top"
})

minetest.register_node("thin_decor:wood_slab_stair", {
        description = "Wooden Slab Stair",
	inventory_image = "default_wood.png",
	wield_image = "default_wood.png",

        tiles = { "default_wood.png" },
	drawtype = "nodebox",
	paramtype2 = "facedir",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.1, -0.5,	0.5, 0, 0},
			{-0.5, 0.4, 0, 		0.5, 0.5, 0.5},
		}
	},
	is_ground_content = true,
	groups = {choppy=1},
	drop = "thin_decor:wood_slab_stair"
})

