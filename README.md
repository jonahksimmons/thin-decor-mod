# Thin Decor
Mod for [Minetest](https://www.minetest.net/) that adds thinner (0.1 nodes thick) building nodes.

## Features
Currently there are the following node types:
- walls
- floor
- ceiling
- stairs

for the following materials:
- apple wood


## Compatability
This is built for the base game of Minetest. It uses the default texture names so it should be compatable with all texture packs.

